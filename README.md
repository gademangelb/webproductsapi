# Web products catalogue 

# ---- start 

npm run start 

# antes del despliegue en heroku 

crear instancia de redis en heroku


# configuraciones previas para deploy en heroku 


password y url de heroku-redis

const PORT = process.env.PORT || 5000;
app.listen(PORT);

"engines": {
  "node": "10.16.0",
  "npm": "6.9.0"
},"

"scripts": {
  "start": "node app.js"
},

# despliegue en heroku 

git init
git add .
git commit -m "Initial commit"

heroku login

heroku create

git remote add heroku <repositorio git heroku>

git push heroku master

# codigos de error 

400, 'the contentType was not found'    missing header 'content-type'
400, 'credentials corrupt'              decode credentials error
400, 'simulado'                         ten percent of simulated error 
401, 'Authorization was not found'      acces token not found
401, 'token expired'                    acces token expired
403, 'user or pass incorrect'           bad credentials in header 'x-credentials'
403, 'token not found'                  bad request header 'Authorization' not found 
404, 'credentials was not found         bad request header 'x-credentials' not found