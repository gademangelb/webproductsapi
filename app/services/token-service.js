var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('../config/config');
const tokenService = {

  createToken (user) {
    console.info('Create token token-service');
    var payload = {
      sub: user.username,
      iat: moment().unix(),
      exp: moment().add(300, "second").unix(),
    };
    const token = jwt.encode(payload, config.tokenParaphrase);
    return token;
  },

  validateToken (req) {
    console.info('Decode token token-service');
    const token = req.headers.authorization.split(" ")[1];    
    const decode = jwt.decode(token, config.tokenParaphrase);
    return decode;
  }

}
module.exports = tokenService;

