const request = require('request');

const productService = {
    respuesta: '',
    hostname: 'https://simple.ripley.cl/api/v2/',
    pathProducts: 'products?partNumbers=2000373857964P,%202000372163769P,%202000374380478P,%202000375753257P,%202000374030854P,%202000369651873P,%202000374031011P,%20MPM00002448608,%202000371937873P,%202000374030892P,%202000369269023P,%202000369098845P,%202000374575638P,%202000373749665P,%202000374030984P,%202000373583092P,%202000375066210P,%202000374180207P,%202000363223731P,%202000373939745P,%20MPM00002448836,%202000372089564P,%202000374030847P,%20MPM00002031190,%202000374030960P,%202000374341103P,%202000373868861P,%202000373749672P,%202000372026989P,%202000374206006P&format=json',
    pathProductById: '/products/by-id/',
    async getProducts() {
        console.info('product-service Productos');
        try {
            await request(`${this.hostname}${this.pathProducts}`, (err,res,body) =>{
                this.respuesta = body;
                
            });
        } catch (error) {
            throw error.message;
        }
        return this.respuesta;
    },

    async getProductById(productId) {
        console.info('product-service Producto by id ', productId);
        try {
            await request(`${this.hostname}${this.pathProductById}${productId}`, (err,res,body) =>{
                this.respuesta = body;                
            });
        } catch (error) {
            throw error.message;
        }
        return this.respuesta;
    }
}
module.exports = productService;