const config = require('../config/config');
const redis = require('redis');
const redisClient = redis.createClient(config.redis.url, {no_ready_check: true});
redisClient.auth(config.redis.pass);

const servisRedis = {    
    
    saveProducts(products) {
        try {
            console.info('guardar productos en redis');
            redisClient.set('productos', products, 'ex', 60)
            return 'ok';    
        } catch (error) {
            throw error.message;
        }        
    },
    
    getProducts() {
        console.info('getProducts');
        return new Promise((resolve, reject) => {
          redisClient.get('productos', function (err, data) {
            if (err) {
              reject(err);
            }
            resolve(data);
          });
        })
    }    
}
module.exports = servisRedis;
