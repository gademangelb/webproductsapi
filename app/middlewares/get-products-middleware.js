const GetProductsController = require('../controllers/get-products-controller');
const setResponseWithError = require('../util/common-response').setResponseWithError;
const setResponseRaw = require('../util/common-response').setResponseRaw;

module.exports.getProductsMiddleware = async (req, res) => {
  try {
    console.info('getProductsMiddleware'); 
    const rdm = Math.random()*100;
      if (rdm > 0 && rdm < 10) {
        return setResponseWithError(res, 400,'simulado')
      }       
    const controller = new GetProductsController();
    const response = await controller.getProducts();
    return setResponseRaw(res, 200, response);
  } catch (e) {
    return setResponseWithError(res, 500, e);
  }
};
