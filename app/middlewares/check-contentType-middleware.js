const setResponseWithError = require('../util/common-response').setResponseWithError;

module.exports.checkContentType = (req, res, next) => {
  console.info('checkContentType');
  if (req.headers['content-type']){
    console.info('content-type ok ');
    return next();
  }
  console.info('bad content-type ');
  return setResponseWithError(res, 400, 'the contentType was not found');
};
