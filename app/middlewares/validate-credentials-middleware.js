const setResponseWithError = require('../util/common-response').setResponseWithError;
const config = require('../config/config');

module.exports.validateCredentialsMiddleware = async (req, res, next) => {
    console.info('validateCredentialsMiddleware');
    const user = req.user;
    if (user.username === config.credentials.frontUser && 
            user.pass === config.credentials.frontPass) {
                return next();
            }    
    return setResponseWithError(res, 403, 'user or pass incorrect')
}