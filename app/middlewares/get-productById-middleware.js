const setResponseWithError = require('../util/common-response').setResponseWithError;
const setResponseRaw = require('../util/common-response').setResponseRaw;

const ProductController = require('../controllers/get-productById-controller');

module.exports.getProductByIdMiddleware = async (req, res) => {    
    console.info('getProductByIdMiddleware');
    const productId = req.params.productId;
    console.info(req)
    if(!productId) {
        return setResponseWithError(res, 204, 'product was not found');
    }
    try {
        const productController = new ProductController();
        const product = await productController.getProductById(productId);
        
        return setResponseRaw(res, 200, product);
  } catch (e) {
        return setResponseWithError(res, 500, e);
  }
}


