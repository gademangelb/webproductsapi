const setResponseWithError = require('../util/common-response').setResponseWithError;

module.exports.checkCredentialsMiddleware = async (req, res, next) => {
    console.info('checkCredentialsMiddleware');
    const credentials = req.header('x-credentials');
    if (!credentials) {
        return setResponseWithError(res, 404, 'credentials was not found');
    }

    try {
        // eslint-disable-next-line no-undef
        const user = JSON.parse(Buffer.from(credentials, 'base64').toString());
        req.user = user;
      } catch (e) {
        return setResponseWithError(res, 400, 'credentials corrupt',);
      }
      return next();        
}






  