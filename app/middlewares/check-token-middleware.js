const setResponseWithError = require('../util/common-response').setResponseWithError;

module.exports.checkTokenMiddleware = async (req, res, next) => {
    console.info('checkTokenMiddleware');
    const accesToken = req.header('Authorization');
    if (!accesToken) {
        return setResponseWithError(res, 401, 'Authorization was not found');
    }
    return next();        
}

