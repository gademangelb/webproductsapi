const setResponseWithError = require('../util/common-response').setResponseWithError;
const GetTokenController = require('../controllers/create-token-controller');

module.exports.validateTokenMiddleware = async (req, res, next) => {
    console.info('validateTokenMiddleware');      

    if (!req.header('Authorization')){
        return setResponseWithError(res, 403, 'token not found');  
    }        
    const controller = new GetTokenController(); 
    try {
        await controller.validateToken(req);  
    } catch (error) {
        return setResponseWithError(res, 401, 'token expired'); 
    }            
    return next();    
}   


