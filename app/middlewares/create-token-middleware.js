const CreateTokenController = require('../controllers/create-token-controller');
const setResponseWithError = require('../util/common-response').setResponseWithError;
const setResponseRaw = require('../util/common-response').setResponseRaw;

module.exports.createTokenMiddleware = async (req, res) => {
  try {
    console.info('getTokenMiddleware');
    const controller = new CreateTokenController();
    const response = await controller.createToken(req);
    return setResponseRaw(res, 200, response);
  } catch (e) {
    return setResponseWithError(res, 500, e);
  }
};
