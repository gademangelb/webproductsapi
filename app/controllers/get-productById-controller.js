const productService = require('../services/product-service');

module.exports = function () {
  this.getProductById = async (productId) => {
    console.info('productService productId: ', productId)
    // eslint-disable-next-line no-useless-catch
    try {      
      console.info('ProductService getProductById');      
      const producto = await productService.getProductById(productId);
      console.info('producto: ', producto);
      return this.createResponse(producto);

    } catch (e) {
      throw e;
    }
  }
  this.createResponse = (response) => {
    if (response) {
      return {
        code: 'ok',
        message: response
      };
    }
    return {
      code: 'not found',
      message: {}
    };
  };
};
