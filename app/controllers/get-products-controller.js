const productService = require('../services/product-service');
const redisService = require('../services/redis-service');

module.exports = function () {
  this.getProducts = async () => {
    // eslint-disable-next-line no-useless-catch
    try {      
      console.info('getProductsController');

      const redisProducts = await redisService.getProducts();
      
      if (redisProducts) {
        console.info('Redis productos');
        return this.createResponse(redisProducts);
      }
      console.info('Ripley productos');
      const productos = await productService.getProducts();
      redisService.saveProducts(productos);
      return this.createResponse(productos);

    } catch (e) {
      throw e;
    }
  }
  this.createResponse = (response) => {
    if (response) {
      return {
        code: 'ok',
        message: response
      };
    }
    return {
      code: 'not found',
      message: {}
    };
  };
};
