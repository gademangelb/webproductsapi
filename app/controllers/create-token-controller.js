const serviceToken = require('../services/token-service');

module.exports = function () {    
    this.createToken = async (req) => {
        console.info('createTokenController, createToken')
        const user = req.user; 
        const token = serviceToken.createToken(user); 
        return this.createResponse(token);         
    }
    this.validateToken = async (req) => {
        console.info('createTokenController, validateToken')
        const resp = await serviceToken.validateToken(req)
        return resp;
    }
    this.createResponse = (response) => {
        if (response) {
            return {
                code: 'ok',
                message: response
            }
        }
        return {
            code: 'bad response',
            message: 'something go wrong'
        }
    }    
}