const Router = require('express').Router;
const getProductsMiddleware = require('../middlewares/get-products-middleware').getProductsMiddleware;
const checkContentTypeMiddleware = require('../middlewares/check-contentType-middleware').checkContentType;
const createTokenMiddleware = require('../middlewares/create-token-middleware').createTokenMiddleware;
const checkCredentialsMiddleware = require('../middlewares/check-credentials-middleware').checkCredentialsMiddleware;
const validateCredentialsMiddleware = require('../middlewares/validate-credentials-middleware').validateCredentialsMiddleware;
const validateTokenMiddleware = require('../middlewares/validate-token-middleware').validateTokenMiddleware;
const getProductByIdMiddleware = require('../middlewares/get-productById-middleware').getProductByIdMiddleware;


const router = Router();
router.get('/login', [checkContentTypeMiddleware, checkCredentialsMiddleware, validateCredentialsMiddleware, createTokenMiddleware]);
router.get('/all', [checkContentTypeMiddleware, validateTokenMiddleware, getProductsMiddleware]); 
router.get('/byId/:productId', [checkContentTypeMiddleware, getProductByIdMiddleware]); 
console.info('product-route');
module.exports = router;