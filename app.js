const express = require('express'); 
const displayRoutes = require('express-routemap');
const app = express();
const cors = require('cors');
const productsRout = require('./app/routes/products-route');
const config = require('./app/config/config');

app.use(cors());

app.use('/api/products/', productsRout);  


app.use((err, req, res, next) => { // eslint-disable-line
  res.status(500).send({ code: 'error', message: 'internal error not handled' });
});

app.listen(config.port, () => {
    console.info("Server started in port 3050");
    displayRoutes(app);
})

